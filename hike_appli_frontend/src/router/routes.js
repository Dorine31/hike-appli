
const routes = [
  {
    path: '/',
    component: () => import('layouts/MobileLayout.vue'),
    children: [
      { path: '', component: () => import('pages/mobile/Index.vue') },
      { path: 'search', component: () => import('pages/mobile/Search.vue') },
      { path: 'dashboard', component: () => import('pages/mobile/Dashboard.vue') }
    ],
    beforeEnter (to, from, next) {
      const isMobile = window.matchMedia('only screen and (max-width: 760px)')
      if (!isMobile.matches) {
        next('/desktop')
      } else {
        next()
      }
    }
  },
  {
    path: '/desktop',
    component: () => import('layouts/DesktopLayout.vue'),
    children: [
      { path: '', component: () => import('pages/desktop/Index.vue') },
      { path: 'search', component: () => import('pages/desktop/Search.vue') }
    ],
    beforeEnter (to, from, next) {
      const isMobile = window.matchMedia('only screen and (max-width: 760px)')
      if (isMobile.matches) {
        next('/')
      } else {
        next()
      }
    }
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
