import axios from 'axios'

// import convert from 'xml-js'
// component map
export function createAGeojsonFeature ({ commit }, coordinates) {
  commit('CREATE_GEOJSON_FEATURE', coordinates)
}

export function uploadGPXURL ({ commit, state }, hike) {
  const gpxURL = `src/statics/${hike.trace_gps.slice(2)}`
  // TODO reprendre cette fonction et faire en sorte que
  //  TODO l'on puisse telecharger une trace gpx
  fetch(gpxURL)
    .then(response => console.log('fetch', JSON.stringify(response)))
  console.log('yes')
  /* var oReq = new XMLHttpRequest()
  oReq.open('GET', 'src' + gpxURL, true)
  oReq.responseType = 'arraybuffer'
  // let byteString = ''
  let byteArray = []
  let arrayBuffer = ''

  oReq.onload = oEvent => {
    arrayBuffer = oReq.response // Note: not oReq.responseText
    if (arrayBuffer) {
      byteArray = new Uint8Array(arrayBuffer)
      console.log('byteArray', byteArray)
    }
  }
  oReq.send()

  function arrayBufferToString (buffer, encoding, callback) {
    var blob = new Blob([buffer], { type: 'text/plain' })
    var reader = new FileReader()
    reader.onload = function (evt) {
      callback(evt.target.result)
    }
    reader.readAsText(blob, encoding)
  }
  arrayBufferToString(byteArray, 'UTF-8', console.log.bind(console)) */
  /* const xml = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
  <gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd" version="1.1" creator="VisuGPX">
  <metadata>
    <name>Palairac</name>
  </metadata></gpx>`
  // a tester ici
  const rawJSON = convert.xml2json(xml, { compact: true, spaces: 4 })
  const convertedJSON = JSON.parse(rawJSON)
  console.log(convertedJSON) */
  commit('UPLOAD_TRACE', gpxURL)
}
// Components search
export function updateLastHikes ({ commit }, biblio) {
  commit('UPDATE_LAST_HIKES', biblio)
}
export function updateInput ({ commit }, e) {
// Valide la mutation et y passe les données
  commit('UPDATE_INPUT', e)
}

export function loadData ({ commit, state }) {
  const url = 'https://api-adresse.data.gouv.fr/search/?q=' + state.inputValue
  axios.get(url)
    .then(response => commit('LOAD_DATA', response.data))
}

export function selectHikes ({ commit }, lat1, long1) {
  commit('selectHikes', lat1, long1)
}
