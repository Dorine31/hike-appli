export default function () {
  return {
  //   mapboxAppli: {
  //     url: 'https://www.visugpx.com',
  //     token: 'JFV7ZFhdJqwQZ28MVDN4hJOFo',
  //     getList: '/api/activities',
  //     getOne: '/api/activity/XykHFK6egf',
  //     activities: 'rp',
  //     cle: 'k2UueZupIQgUqBVo0DMxzB9JYc8toB7a'
  //   },

    // component map
    accessToken: 'pk.eyJ1IjoiZG9yaW5lMzEiLCJhIjoiY2tqbmEyZTBlMjlsazJ0cW9wcjh5OXk2ciJ9.TWFNiMaWoE2mhV7vU6l9HQ',
    mapboxURL: 'mapbox://styles/mapbox/',
    layerID: 'outdoors-v11',
    center: [2.213749, 46.227638],
    geojson: {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: []
      }
    },
    // Components search
    lastHikes: [],
    inputValue: '',
    cityName: '',
    responseData: '',
    hikes: [],
    coordinates: [],
    // gpx: '',
    gpxURL: ''
    // url: ''
  }
}
