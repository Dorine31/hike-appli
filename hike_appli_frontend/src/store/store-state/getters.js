import { biblio } from '../biblio'

// component map
export function getGeojson (state) {
  return state.geojson
}
// Components search
export function getHikeFounded (state) {
  if (state.inputValue && state.cityName) {
    return (state.cityName !== '' && state.hikes !== undefined && state.hikes.length > 0)
  } else {
    return false
  }
}

// for each hike stored in the database,
// retrieve the hike (s) with a distance of less than 10km
export function getLocalHikes (state, lat1, long1) {
  const hikes = []
  biblio.filter(hike => {
    const lat2 = hike.start_latlng[0]
    const long2 = hike.start_latlng[1]
    // lat : 43.72... and long : 1.64...
    const rayon = 6371 // radius of the earth in km
    // degrees to radians converter

    const conversion = Math.PI / 180
    const a = lat1 * conversion
    const b = lat2 * conversion
    const c = long1 * conversion
    const d = long2 * conversion
    const dist = (
      rayon *
      Math.acos(
        Math.sin(a) * Math.sin(b) +
          Math.cos(a) * Math.cos(b) * Math.cos(c - d)
      )
    )
    if (dist <= 10) {
      hikes.push(hike)
    }
  })
  console.log('fini', hikes)
  return hikes
}

export function getHikes (state) {
  console.log('dans getHikes', state.hikes)
  return state.hikes
}
export function getLastHikes (state) {
  return state.lastHikes
}
