// import { getLocalHikes } from './getters'
import { biblio } from '../biblio'
// import state from './state'

// component map

export function CREATE_GEOJSON_FEATURE (state, point) {
  const geojson = { ...state.geojson }
  const coordinates = point.start_latlng.reverse()
  const title = point.titre
  const feature = {
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates: coordinates
    },
    properties: {
      title: title
    }
  }
  geojson.data.features.push(feature)
  state.geojson = geojson
}

export function UPLOAD_TRACE (state, gpxURL) {
  state.gpxURL = gpxURL
  console.log('state', state.gpxURL)
}

// Components search
export function UPDATE_LAST_HIKES (state) {
  biblio.sort(function compare (a, b) {
    if (a.date < b.date) {
      return -1
    }
    if (a.date > b.date) {
      return 1
    }
    return 0
  })
  biblio.map(hike => {
    state.lastHikes.push(hike)
  })
}

export function UPDATE_INPUT (state, message) {
  state.inputValue = message
}

export function LOAD_DATA (state, data) {
  // use the response data
  state.coordinates = data.features[0].geometry.coordinates
  const lat1 = state.coordinates[1]
  const long1 = state.coordinates[0]
  // fill the name of the city
  state.cityName = state.inputValue
  const hikes = []
  // get the hikes with a distance less than 10km
  biblio.filter(hike => {
    const lat2 = hike.start_latlng[0]
    const long2 = hike.start_latlng[1]
    // lat : 43.72... and long : 1.64...
    const rayon = 6371 // radius of the earth in km
    // degrees to radians converter

    const conversion = Math.PI / 180
    const a = lat1 * conversion
    const b = lat2 * conversion
    const c = long1 * conversion
    const d = long2 * conversion
    const dist = (
      rayon *
      Math.acos(
        Math.sin(a) * Math.sin(b) +
          Math.cos(a) * Math.cos(b) * Math.cos(c - d)
      )
    )
    if (dist <= 10) {
      hikes.push(hike)
    }
  })
  console.log('hikes', hikes)
  state.hikes = hikes
  console.log('state', state.hikes)
}
